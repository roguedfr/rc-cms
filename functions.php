<?php
add_theme_support( 'custom-logo' );
add_theme_support( 'menus' );

function portfolio_post() { 
	// creating (registering) the custom type 
	register_post_type( 'portfolio', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Portfolio', 'escms'), /* This is the Title of the Group */
			'singular_name' => __('Portfolio', 'escms'), /* This is the individual type */
			'all_items' => __('Portfolio', 'escms'), /* the all items menu item */
			'add_new' => __('Add New', 'escms'), /* The add new menu item */
			'add_new_item' => __('Add New Portfolio', 'escms'), /* Add New Display Title */
			'edit' => __( 'Edit', 'escms' ), /* Edit Dialog */
			'edit_item' => __('Edit Portfolio', 'escms'), /* Edit Display Title */
			'new_item' => __('New Portfolio', 'escms'), /* New Display Title */
			'view_item' => __('View Portfolio', 'escms'), /* View Display Title */
			'search_items' => __('Search Portfolio', 'escms'), /* Search Custom Type Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'escms'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'escms'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Portfolio', 'escms' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 100, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-universal-access', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'Portfolio', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'Portfolio', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'show_in_rest' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'custom_type');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'custom_type');
	
} 


add_action( 'init', 'portfolio_post');

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Theme General Settings'),
            'menu_title'    => __('Theme Settings'),
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
    }
}